package com.example.shopproject.Model;

public class MemberRes {
    String nama, ket;

    public MemberRes(String nama, String ket) {
        this.nama = nama;
        this.ket = ket;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }
}
