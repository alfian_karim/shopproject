package com.example.shopproject.Model;

import java.util.List;

public class PesanToAdmin {
    public List<Pesanan> pesanan;
    public String status;
    public long total;
    public String pnamaPemesan;
    public String ppemail, tanggal, key;

    public PesanToAdmin() {
    }

    public PesanToAdmin(List<Pesanan> pesanan, long total, String pnamaPemesan, String ppemail, String tanggal, String status) {
        this.pesanan = pesanan;
        this.total = total;
        this.pnamaPemesan = pnamaPemesan;
        this.ppemail = ppemail;
        this.tanggal = tanggal;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPpemail() {
        return ppemail;
    }

    public void setPpemail(String ppemail) {
        this.ppemail = ppemail;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public List<Pesanan> getPesanan() {
        return pesanan;
    }

    public void setPesanan(List<Pesanan> pesanan) {
        this.pesanan = pesanan;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getPnamaPemesan() {
        return pnamaPemesan;
    }

    public void setPnamaPemesan(String pnamaPemesan) {
        this.pnamaPemesan = pnamaPemesan;
    }
}
