package com.example.shopproject.Model;

import android.net.Uri;

public class Barang {
    public String tanggal, key;
    public String nama, harga, jenis, jumlah, urlImage;
    public int idSpinner;
    public String uriIamge;

    public Barang() {
    }

    public Barang(String tanggal, String nama, String harga, String jenis, String jumlah, String urlImage, int idSpinner, String uri) {
        this.tanggal = tanggal;
        this.nama = nama;
        this.harga = harga;
        this.jenis = jenis;
        this.jumlah = jumlah;
        this.urlImage = urlImage;
        this.idSpinner = idSpinner;
        this.uriIamge = uri;
    }

    public int getIdSpinner() {
        return idSpinner;
    }

    public String getUriIamge() {
        return uriIamge;
    }

    public void setUriIamge(String uriIamge) {
        this.uriIamge = uriIamge;
    }

    public void setIdSpinner(int idSpinner) {
        this.idSpinner = idSpinner;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
