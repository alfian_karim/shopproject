package com.example.shopproject.Member;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterKeranjang;
import com.example.shopproject.Admin.TambahBarang;
import com.example.shopproject.Model.Barang;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.Model.Pesanan;
import com.example.shopproject.R;
import com.example.shopproject.Splashscreen2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class KeranjangActivity extends AppCompatActivity {

    RecyclerView rv_keranjang;
    public static AdapterKeranjang adapterKeranjang;
    public static List<Pesanan> pesananList = new ArrayList<>();
    public static TextView tv_total;
    public static long jml;
    private EditText namaPemesan;
    private DatabaseReference mDatabaseRef, mDatabaseRef2;
    private FirebaseAuth fAuth;
    ProgressDialog loading;
    PesanToAdmin pesan;
    Button checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);
        rv_keranjang = findViewById(R.id.rv_keranjang);
        tv_total = findViewById(R.id.tv_total);
        checkout = findViewById(R.id.btn_checkout);

        adapterKeranjang = new AdapterKeranjang(pesananList, KeranjangActivity.this);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(KeranjangActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_keranjang.setLayoutManager(linearLayoutManager1);
        rv_keranjang.setAdapter(adapterKeranjang);

        getTotalBelanja();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("PesananMember");
        mDatabaseRef2 = FirebaseDatabase.getInstance().getReference("SemuaPesananMember");
        fAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = fAuth.getCurrentUser();
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());


        pesan = new PesanToAdmin(pesananList, jml, user.getDisplayName(), user.getEmail(), currentDate, "ongoing");

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadToAdmin(pesan);
                uploadToMember(pesan);
                pesananList.clear();
                jml=0;
                startActivity(new Intent(KeranjangActivity.this, Splashscreen2.class));
                finish();
            }
        });

    }

    private void uploadToAdmin(PesanToAdmin pesanToAdmin){
        loading = ProgressDialog.show(
                KeranjangActivity.this,
                null,
                "Loading...",
                true,
                true
        );
        String uploadId = mDatabaseRef.push().getKey();
        mDatabaseRef.child(uploadId).setValue(pesanToAdmin);
        Log.i("TAG", "uploadToAdmin: "+fAuth.getCurrentUser().getDisplayName());

        Toast.makeText(KeranjangActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
        loading.dismiss();
    }

    private void uploadToMember(PesanToAdmin pesanToAdmin){
        loading = ProgressDialog.show(
                KeranjangActivity.this,
                null,
                "Loading...",
                true,
                true
        );
        String uploadId = mDatabaseRef.push().getKey();
        mDatabaseRef2.child(fAuth.getCurrentUser().getDisplayName()).child(uploadId).setValue(pesanToAdmin);

        Toast.makeText(KeranjangActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
        loading.dismiss();
    }

    @Override
    protected void onResume() {
        adapterKeranjang = new AdapterKeranjang(pesananList, KeranjangActivity.this);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(KeranjangActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_keranjang.setLayoutManager(linearLayoutManager1);
        rv_keranjang.setAdapter(adapterKeranjang);

        super.onResume();
    }

    private void getTotalBelanja(){
        jml = 0;
        for(Pesanan pesanan : pesananList){
            jml = (pesanan.jumlah * pesanan.harga) + jml;
        }
        tv_total.setText(String.valueOf(jml));
    }


}