package com.example.shopproject.Member;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterBarang;
import com.example.shopproject.Adapter.AdapterHystory;
import com.example.shopproject.Adapter.AdapterOrder;
import com.example.shopproject.Adapter.AdapterOrderMember;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HystoryFragment extends Fragment {

    RecyclerView rv_order;
    AdapterOrderMember adapterOrder;
    DatabaseReference fDatabase;
    List<PesanToAdmin> list = new ArrayList<>();
    private ValueEventListener mDBListener;
    private FirebaseAuth fAuth;
    ProgressDialog loading;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hystory, container, false);
        rv_order = root.findViewById(R.id.rv_hystory);
        swipeRefreshLayout = root.findViewById(R.id.swiperefresh);

        fAuth = FirebaseAuth.getInstance();
        fDatabase = FirebaseDatabase.getInstance().getReference("SemuaPesananMember").child(fAuth.getCurrentUser().getDisplayName());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        onLoading();

        return root;
    }

    @Override
    public void onResume() {
        onLoading();
        super.onResume();
    }

    private void onLoading() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        });
    }

    private void getData(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    PesanToAdmin upload = ktpSnapshot.getValue(PesanToAdmin.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                adapterOrder = new AdapterOrderMember(list, getActivity());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                rv_order.setLayoutManager(linearLayoutManager);
                rv_order.setAdapter(adapterOrder);
                adapterOrder.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        swipeRefreshLayout.setRefreshing(false);
        loading.dismiss();
    }
}