package com.example.shopproject.Member;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterBarang;
import com.example.shopproject.Adapter.AdapterBarangBeli;
import com.example.shopproject.Admin.TambahBarang;
import com.example.shopproject.Model.Barang;
import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BelanjaFragment extends Fragment implements View.OnClickListener{
    RecyclerView rv_barang;
    AdapterBarangBeli adapterBarang;
    List<Barang> list = new ArrayList<>();
    CardView cv_rokok,cv_jajan,cv_minum,cv_nasi,cv_popok;
    ProgressDialog loading;
    DatabaseReference fDatabase;
    private ValueEventListener mDBListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_belanja, container, false);
        rv_barang = root.findViewById(R.id.rv_barang);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_barang.setLayoutManager(linearLayoutManager1);
        cv_rokok = root.findViewById(R.id.cv_rokok);
        cv_jajan = root.findViewById(R.id.cv_jajan);
        cv_minum = root.findViewById(R.id.cv_minuman);
        cv_nasi = root.findViewById(R.id.cv_nasi);
        cv_popok = root.findViewById(R.id.cv_popok);

        backgorundColor(1);

        cv_rokok.setOnClickListener(this);
        cv_jajan.setOnClickListener(this);
        cv_minum.setOnClickListener(this);
        cv_nasi.setOnClickListener(this);
        cv_popok.setOnClickListener(this);


        return root;
    }

    public void backgorundColor(int i){
        switch (i){
            case 1:
                cv_rokok.setBackgroundColor(getContext().getResources().getColor(R.color.biru_dasar));
                cv_jajan.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_minum.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_nasi.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_popok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                getListRokok();
                break;
            case 2:
                cv_rokok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_jajan.setBackgroundColor(getContext().getResources().getColor(R.color.biru_dasar));
                cv_minum.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_nasi.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_popok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                getListBahanPokok();
                break;
            case 3:
                cv_rokok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_jajan.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_minum.setBackgroundColor(getContext().getResources().getColor(R.color.biru_dasar));
                cv_nasi.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_popok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                getListJajan();
                break;
            case 4:
                cv_rokok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_jajan.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_minum.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_nasi.setBackgroundColor(getContext().getResources().getColor(R.color.biru_dasar));
                cv_popok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                getListMinuman();
                break;
            case 5:
                cv_rokok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_jajan.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_minum.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_nasi.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                cv_popok.setBackgroundColor(getContext().getResources().getColor(R.color.biru_dasar));
                getListPopok();
                break;
        }
    }

    private void getListRokok(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        list.clear();
        fDatabase = FirebaseDatabase.getInstance().getReference("Rokok");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Barang upload = ktpSnapshot.getValue(Barang.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarang = new AdapterBarangBeli(list, getActivity());
                rv_barang.setAdapter(adapterBarang);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getListMinuman(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        list.clear();
        fDatabase = FirebaseDatabase.getInstance().getReference("Minuman");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Barang upload = ktpSnapshot.getValue(Barang.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarang = new AdapterBarangBeli(list, getActivity());
                rv_barang.setAdapter(adapterBarang);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getListJajan(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        list.clear();
        fDatabase = FirebaseDatabase.getInstance().getReference("Jajan");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Barang upload = ktpSnapshot.getValue(Barang.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarang = new AdapterBarangBeli(list, getActivity());
                rv_barang.setAdapter(adapterBarang);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getListBahanPokok(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        list.clear();
        fDatabase = FirebaseDatabase.getInstance().getReference("Bahan Pokok");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Barang upload = ktpSnapshot.getValue(Barang.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarang = new AdapterBarangBeli(list, getActivity());
                rv_barang.setAdapter(adapterBarang);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getListPopok(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        list.clear();
        fDatabase = FirebaseDatabase.getInstance().getReference("Popok");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Barang upload = ktpSnapshot.getValue(Barang.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarang = new AdapterBarangBeli(list, getActivity());
                rv_barang.setAdapter(adapterBarang);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cv_rokok:
                backgorundColor(1);
                break;
            case R.id.cv_jajan:
                backgorundColor(2);
                break;
            case R.id.cv_minuman:
                backgorundColor(3);
                break;
            case R.id.cv_nasi:
                backgorundColor(4);
                break;
            case R.id.cv_popok:
                backgorundColor(5);
                break;
        }
    }
}