package com.example.shopproject.Member;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.shopproject.Admin.DashboardAdmin;
import com.example.shopproject.Admin.HomeFragment;
import com.example.shopproject.Admin.ListOrderFragment;
import com.example.shopproject.Admin.LoginActivity;
import com.example.shopproject.Admin.MemberFragment;
import com.example.shopproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

public class DashboardMember extends AppCompatActivity {
    public static ChipNavigationBar btn_nav;
    FragmentManager fragmentManager;
    ImageView iv_chart;
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_member);
        btn_nav = findViewById(R.id.bottomNav);
        iv_chart = findViewById(R.id.iv_chart);
        logout = findViewById(R.id.btn_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(DashboardMember.this, LoginMember.class));
                finish();
            }
        });

        if (savedInstanceState == null) {
            btn_nav.setItemSelected(R.id.brlanja, true);
            fragmentManager = getSupportFragmentManager();
            BelanjaFragment homeFragment = new BelanjaFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, homeFragment)
                    .commit();
        }

        btn_nav.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {

                Fragment fragment = null;
                switch (i) {
                    case R.id.brlanja:
                        fragment = new BelanjaFragment();
                        break;
                    case R.id.hystory:
                        fragment = new HystoryFragment();
                        break;
                }

                if (fragment != null) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .commit();
                }
            }
        });

        iv_chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardMember.this,KeranjangActivity.class));
            }
        });
    }
}