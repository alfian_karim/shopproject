package com.example.shopproject.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterBarang;
import com.example.shopproject.Adapter.AdapterMember;
import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.Model.Member;
import com.example.shopproject.Model.MemberRes;
import com.example.shopproject.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MemberFragment extends Fragment {
    ImageView iv_plus;
    RecyclerView rv_member;
    AdapterMember adapterMember;
    List<Member> list = new ArrayList<>();
    DatabaseReference fDatabase;
    private ValueEventListener mDBListener;
    ProgressDialog loading;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_member, container, false);;
        iv_plus = root.findViewById(R.id.iv_plus_member);
        swipeRefreshLayout = root.findViewById(R.id.swiperefresh);
        rv_member = root.findViewById(R.id.rv_member);
        rv_member.setHasFixedSize(true);
        rv_member.setLayoutManager(new LinearLayoutManager(getActivity()));

        iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),TambahMember.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListMember();
            }
        });

        onLoading();

        return root;
    }

    @Override
    public void onResume() {
        onLoading();
        super.onResume();
    }

    private void onLoading() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getListMember();
            }
        });
    }

    private void getListMember(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );

        fDatabase = FirebaseDatabase.getInstance().getReference("Data Member");

        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Member upload = ktpSnapshot.getValue(Member.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }

                adapterMember = new AdapterMember(list, getActivity());
                rv_member.setAdapter(adapterMember);
                adapterMember.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        loading.dismiss();
        swipeRefreshLayout.setRefreshing(false);
    }

}