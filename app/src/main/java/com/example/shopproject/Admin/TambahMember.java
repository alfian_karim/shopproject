package com.example.shopproject.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shopproject.Model.Member;
import com.example.shopproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TambahMember extends AppCompatActivity {
    FirebaseAuth fAuth;
    DatabaseReference fDatabase;
    ProgressDialog loading;
    EditText nama, email, alamat, nomerHp, pass;
    Button tambah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_member);

        nama = findViewById(R.id.et_nama);
        email = findViewById(R.id.et_email);
        alamat = findViewById(R.id.et_alamat);
        nomerHp = findViewById(R.id.et_nomor);
        pass = findViewById(R.id.et_pass);
        tambah = findViewById(R.id.btn_tambah);

        fAuth = FirebaseAuth.getInstance();
        fDatabase = FirebaseDatabase.getInstance().getReference("Data Member");

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createMember();
            }
        });

    }

    private void createMember(){
        String emaill = email.getText().toString().trim();
        String passwordd = pass.getText().toString().trim();

        if(TextUtils.isEmpty(emaill)){
            email.setError("Email is Required.");
            return;
        }

        if(TextUtils.isEmpty(passwordd)){
            pass.setError("Password is Required.");
            return;
        }

        if (passwordd.length()<6){
            pass.setError("password must be more than 6 characters");
            return;
        }

        // register the user in firebase
        loading = ProgressDialog.show(
                TambahMember.this,
                null,
                "Loading...",
                true,
                true
        );

        fAuth.createUserWithEmailAndPassword(emaill, passwordd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(TambahMember.this, "Failed - "+task.getException(), Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(nama.getText().toString()).build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("TAG", "User profile updated.");
                                    }
                                }
                            });
                    loading.dismiss();
                    uploadDataMember();
                }
            }
        });

    }

    private void uploadDataMember(){
        String namaa = nama.getText().toString();
        String emaill = email.getText().toString();
        String alamatt = alamat.getText().toString();
        String nomerHpp = nomerHp.getText().toString();
        String passs = pass.getText().toString();

        if (TextUtils.isEmpty(namaa)){
            Toast.makeText(this, "Lengkapi Form", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(emaill)){
            Toast.makeText(this, "Lengkapi Form", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(alamatt)){
            Toast.makeText(this, "Lengkapi Form", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(nomerHpp)){
            Toast.makeText(this, "Lengkapi Form", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(passs)){
            Toast.makeText(this, "Lengkapi Form", Toast.LENGTH_SHORT).show();
        }
        else {
            loading = ProgressDialog.show(
                    TambahMember.this,
                    null,
                    "Loading...",
                    true,
                    true
            );
            String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

            Member member = new Member(namaa, emaill,alamatt, nomerHpp, passs, currentDate);

            String uploadId = fDatabase.push().getKey();
            fDatabase.child(uploadId).setValue(member);
            Toast.makeText(TambahMember.this, "Akun Berhasil Dibuat", Toast.LENGTH_SHORT).show();
            loading.dismiss();
            finish();
        }


    }
}