package com.example.shopproject.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shopproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class LoginActivity extends AppCompatActivity {

    Button btn_login;
    EditText email, pass;

    FirebaseAuth fAuth;
    ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = findViewById(R.id.btn_login);
        email = findViewById(R.id.et_username);
        pass = findViewById(R.id.et_pass);


        fAuth = FirebaseAuth.getInstance();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAdmin();
            }
        });
    }

    public void loginAdmin(){
        String emaill = email.getText().toString();
        String passs = pass.getText().toString();

        if (TextUtils.isEmpty(emaill)){
            email.setError("Email is Required.");
            return;
        }
        if (TextUtils.isEmpty(passs)){
            pass.setError("Password is Required.");
            return;
        }

        loading = ProgressDialog.show(
                LoginActivity.this,
                null,
                "Loading...",
                true,
                true
        );
        fAuth.signInWithEmailAndPassword(emaill,passs).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    loading.dismiss();
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    Toast.makeText(LoginActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(LoginActivity.this, DashboardAdmin.class);
                    i.putExtra("id", firebaseUser.getUid());
                    startActivity(i);
                    finish();
                }else {
                    Toast.makeText(LoginActivity.this, "Gagal Login", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }
        });
    }

}