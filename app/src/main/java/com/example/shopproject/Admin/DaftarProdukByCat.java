package com.example.shopproject.Admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.shopproject.Adapter.AdapterBarang;
import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.R;

import java.util.ArrayList;
import java.util.List;

public class DaftarProdukByCat extends AppCompatActivity {

    AdapterBarang adapterBarang;
    List<BarangRes> list = new ArrayList<>();
    RecyclerView rv_barang_by_cat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_produk_by_cat);
        rv_barang_by_cat = findViewById(R.id.rv_barangbycat);

        addDataDummy();
    }

    void addDataDummy(){
        for(int i =0; i<6 ; i++){
            BarangRes barangRes = new BarangRes("Djarum","Rp.10000","9");
            list.add(barangRes);
        }
//        adapterBarang = new AdapterBarang(list,DaftarProdukByCat.this);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(DaftarProdukByCat.this, LinearLayoutManager.VERTICAL, false);
        rv_barang_by_cat.setLayoutManager(linearLayoutManager1);
        rv_barang_by_cat.setAdapter(adapterBarang);
    }
}