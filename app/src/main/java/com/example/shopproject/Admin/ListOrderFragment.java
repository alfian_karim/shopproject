package com.example.shopproject.Admin;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterMember;
import com.example.shopproject.Adapter.AdapterOrder;
import com.example.shopproject.Model.Member;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListOrderFragment extends Fragment {
    RecyclerView rv_order;
    AdapterOrder adapterOrder;
    DatabaseReference fDatabase;
    List<PesanToAdmin> list = new ArrayList<>();
    private ValueEventListener mDBListener;
    ProgressDialog loading;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_order, container, false);
        rv_order = root.findViewById(R.id.rv_order);

        swipeRefreshLayout = root.findViewById(R.id.swiperefresh);
        fDatabase = FirebaseDatabase.getInstance().getReference("PesananMember");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        onLoading();


        return root;
    }

    @Override
    public void onResume() {
        getData();
        super.onResume();
    }

    private void onLoading() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        });
    }

    private void getData(){
        loading = ProgressDialog.show(
                getActivity(),
                null,
                "Loading...",
                true,
                true
        );
        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    PesanToAdmin upload = ktpSnapshot.getValue(PesanToAdmin.class);
                    upload.setKey(ktpSnapshot.getKey());
                    list.add(upload);
                }

                adapterOrder = new AdapterOrder(list, getActivity());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                rv_order.setLayoutManager(linearLayoutManager);
                rv_order.setAdapter(adapterOrder);
                adapterOrder.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        loading.dismiss();
        swipeRefreshLayout.setRefreshing(false);
    }
}