package com.example.shopproject.Admin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.shopproject.Adapter.SpinAdapter;
import com.example.shopproject.Model.Barang;
import com.example.shopproject.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UpdateBarang extends AppCompatActivity {
    String nama, harga, jenis, jumlah, urlImage, key;
    int idSpinner;

    ImageView iv_back,iv_barang;
    Button btn_upload, tambah;
    String[] list_spin = {
            "Pilih Kategori",
            "Bahan Pokok",
            "Rokok",
            "Minuman",
            "Jajan",
            "Popok"
    };

    SpinAdapter spinAdapter;
    Spinner spinner;
    Uri resultUri;
    Uri uri;

    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;
    private StorageTask mUploadTask;
    FirebaseAuth fAuth;
    ProgressDialog loading;

    EditText nama1, harga1, jumlah1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_barang);

        nama = getIntent().getStringExtra("nama");
        harga = getIntent().getStringExtra("harga");
        jenis = getIntent().getStringExtra("jenis");
        jumlah = getIntent().getStringExtra("jumlah");
        urlImage = getIntent().getStringExtra("image");
        idSpinner = getIntent().getIntExtra("idSpinner", 0);
        key = getIntent().getStringExtra("key");
        String ri = getIntent().getStringExtra("uri");

        uri = Uri.parse(ri);

        tambah = findViewById(R.id.btn_tambah);
        nama1 = findViewById(R.id.et_nama);
        harga1 = findViewById(R.id.et_harga);
        spinner = findViewById(R.id.spin_cat);
        btn_upload = findViewById(R.id.btn_foto);
        iv_back = findViewById(R.id.iv_back);
        iv_barang = findViewById(R.id.iv_barang);
        jumlah1 = findViewById(R.id.et_jumlah);

        nama1.setText(nama);
        jumlah1.setText(jumlah);
        harga1.setText(harga);
        spinAdapter = new SpinAdapter(UpdateBarang.this,android.R.layout.simple_spinner_dropdown_item,list_spin);
        spinner.setAdapter(spinAdapter);
        spinner.setSelection(idSpinner);
        Log.i("TAG", "onCreate: "+idSpinner);
        Glide.with(this).load(urlImage).into(iv_barang);

        fAuth = FirebaseAuth.getInstance();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCrop();
            }
        });

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadData();
            }
        });


    }

    private void getCrop() {
        CropImage.activity()
                .start(UpdateBarang.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                iv_barang.setImageURI(resultUri);
//                 path = resultUri.ath();
////                String[] sparated = path.split("/");
////                ImageName = sparated[6];
////                Log.i("imagaasdja",sparated[6]);getP
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void  uploadData(){
//
        String name = nama1.getText().toString();
        String price = harga1.getText().toString();
        String jenis = spinner.getSelectedItem().toString();
        int idSpinner = spinner.getSelectedItemPosition();
        if (jenis.equals("Bahan Pokok")){
            mStorageRef = FirebaseStorage.getInstance().getReference("Bahan Pokok");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Bahan Pokok");
        }else if (jenis.equals("Rokok")){
            mStorageRef = FirebaseStorage.getInstance().getReference("Rokok");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Rokok");
        }else if (jenis.equals("Minuman")){
            mStorageRef = FirebaseStorage.getInstance().getReference("Minuman");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Minuman");
        }else if (jenis.equals("Jajan")){
            mStorageRef = FirebaseStorage.getInstance().getReference("Jajan");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Jajan");
        }else if (jenis.equals("Popok")){
            mStorageRef = FirebaseStorage.getInstance().getReference("Popok");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Popok");
        }
        String jumlahh = jumlah1.getText().toString();
        Log.i("TAG", "uploadData: "+jumlahh);
        Log.i("TAG", "uploadData: "+key);

        StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                + ".jpg");

        loading = ProgressDialog.show(
                UpdateBarang.this,
                null,
                "Loading...",
                true,
                true
        );

        if (resultUri==null){

            resultUri = uri;
        }

        Log.i("TAG", "uploadData: "+resultUri);

        mUploadTask = fileReference.putFile(resultUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        mUploadTask.continueWithTask(new Continuation() {
                            @Override
                            public Object then(@NonNull Task task) throws Exception {
                                return fileReference.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                                Barang barang = new Barang(currentDate, name, price, jenis, jumlahh,task.getResult().toString(), idSpinner, resultUri.toString());
                                String uploadId = mDatabaseRef.push().getKey();
                                mDatabaseRef.child(key).setValue(barang);

                                Toast.makeText(UpdateBarang.this, "Berhasil", Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                                finish();
                            }
                        });
                    }
                });
    }
}