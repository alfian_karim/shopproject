package com.example.shopproject.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shopproject.Adapter.AdapterBarangBeli;
import com.example.shopproject.Adapter.AdapterBarangSudahBeli;
import com.example.shopproject.Adapter.AdapterOrder;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.Model.Pesanan;
import com.example.shopproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DetailOrderActivity extends AppCompatActivity {

    RecyclerView rv_barang_beli;
    AdapterBarangSudahBeli adapterBarangSudahBeli;
    List<Pesanan> list = new ArrayList<>();
    DatabaseReference fDatabase;
    private ValueEventListener mDBListener;
    ProgressDialog loading;
    TextView nama, tanggal, total;
    Button kirim;
    FirebaseAuth fAuth;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        rv_barang_beli = findViewById(R.id.rv_barang_beli);
        kirim = findViewById(R.id.btn_kirim);
        nama = findViewById(R.id.tv_nama);
        tanggal = findViewById(R.id.tv_tanggal);
        total = findViewById(R.id.tv_total);
        back = findViewById(R.id.iv_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        long tot = getIntent().getLongExtra("total", 0);
        String name = getIntent().getStringExtra("nama");
        String key = getIntent().getStringExtra("key");
        String tgl = getIntent().getStringExtra("tgl");
        String role = getIntent().getStringExtra("role");
        fAuth = FirebaseAuth.getInstance();

        if (role.equals("admin")){
            fDatabase = FirebaseDatabase.getInstance().getReference("PesananMember").child(key).child("pesanan");
            kirim.setVisibility(View.GONE);
        }else if (role.equals("member")){
            fDatabase = FirebaseDatabase.getInstance().getReference("SemuaPesananMember").child(fAuth.getCurrentUser().getDisplayName()).child(key).child("pesanan");
            kirim.setVisibility(View.GONE);
        }

        nama.setText(name);
        tanggal.setText(tgl);
        total.setText("Rp. "+String.valueOf(tot));


        Log.i("TAG", "onCreate: "+key);

        loading = ProgressDialog.show(
                this,
                null,
                "Loading...",
                true,
                true
        );
        mDBListener = fDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();

                for (DataSnapshot ktpSnapshot : snapshot.getChildren()) {
                    Pesanan upload = ktpSnapshot.getValue(Pesanan.class);
                    list.add(upload);
                }
                loading.dismiss();
                adapterBarangSudahBeli = new AdapterBarangSudahBeli(list, DetailOrderActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DetailOrderActivity.this);
                rv_barang_beli.setLayoutManager(linearLayoutManager);
                rv_barang_beli.setAdapter(adapterBarangSudahBeli);
                adapterBarangSudahBeli.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DetailOrderActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }
}