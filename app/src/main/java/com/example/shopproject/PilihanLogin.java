package com.example.shopproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.shopproject.Admin.LoginActivity;
import com.example.shopproject.Member.LoginMember;

public class PilihanLogin extends AppCompatActivity {

    CardView cv_admin,cv_member;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilihan_login);

        cv_admin = findViewById(R.id.cv_admin);
        cv_member = findViewById(R.id.cv_user);

        cv_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PilihanLogin.this, LoginActivity.class));
            }
        });

        cv_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PilihanLogin.this, LoginMember.class));
            }
        });


    }
}