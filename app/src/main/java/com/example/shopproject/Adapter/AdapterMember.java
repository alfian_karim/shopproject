package com.example.shopproject.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.Model.Member;
import com.example.shopproject.Model.MemberRes;
import com.example.shopproject.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterMember extends RecyclerView.Adapter<AdapterMember.HolderMember> {
    List<Member> list = new ArrayList<>();
    Context context;

    public AdapterMember(List<Member> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterMember.HolderMember onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_member, parent, false);
        return new HolderMember(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMember.HolderMember holder, int position) {
        holder.tv_ket.setText(list.get(position).getAlamat());
        holder.tv_nama.setText(list.get(position).getNama()+" || "+list.get(position).getNomerHp());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderMember extends RecyclerView.ViewHolder {
        TextView tv_nama,tv_ket;
        public HolderMember(@NonNull View itemView) {
            super(itemView);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_ket = itemView.findViewById(R.id.tv_ket);
        }
    }
}
