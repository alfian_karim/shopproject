package com.example.shopproject.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.R;

public class AdapterHystory extends RecyclerView.Adapter<AdapterHystory.HolderHystory>{
    @NonNull
    @Override
    public HolderHystory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order2, parent, false);
        return new HolderHystory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderHystory holder, int position) {
        holder.btn_kirim.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class HolderHystory extends RecyclerView.ViewHolder {
        Button btn_kirim;
        public HolderHystory(@NonNull View itemView) {
            super(itemView);
            btn_kirim = itemView.findViewById(R.id.btn_kirim);
        }
    }
}
