package com.example.shopproject.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Admin.TambahBarang;
import com.example.shopproject.Member.KeranjangActivity;
import com.example.shopproject.Model.Pesanan;
import com.example.shopproject.R;

import java.util.List;

public class AdapterKeranjang extends RecyclerView.Adapter<AdapterKeranjang.HolderKeranjang> {
    List<Pesanan> list;
    Context context;
    long jml;

    public AdapterKeranjang(List<Pesanan> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterKeranjang.HolderKeranjang onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_keranjang, parent, false);
        return new HolderKeranjang(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterKeranjang.HolderKeranjang holder, int position) {
        holder.tv_nm.setText(list.get(position).getNamaBarang());
        holder.tv_hrg.setText(String.valueOf(list.get(position).getHarga()));
        holder.tv_jml.setText(String.valueOf(list.get(position).getJumlah()));
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeranjangActivity.pesananList.remove(position);
                KeranjangActivity.adapterKeranjang.notifyDataSetChanged();

                jml = 0;
                for(Pesanan pesanan : KeranjangActivity.pesananList){
                    jml = (pesanan.jumlah * pesanan.harga)-jml;
                }

                KeranjangActivity.tv_total.setText(String.valueOf(jml));
            }
        });

//        jml = 0;
//        for(Pesanan pesanan : KeranjangActivity.pesananList){
//            jml = (pesanan.jumlah * pesanan.harga)+jml;
//        }
//
//        KeranjangActivity.tv_total.setText(String.valueOf(jml));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderKeranjang extends RecyclerView.ViewHolder {
        TextView tv_jml, tv_nm, tv_hrg;
        ImageView del;
        public HolderKeranjang(@NonNull View itemView) {
            super(itemView);

            del = itemView.findViewById(R.id.delete);
            tv_jml = itemView.findViewById(R.id.tv_jumlah);
            tv_nm = itemView.findViewById(R.id.tv_nama);
            tv_hrg = itemView.findViewById(R.id.tv_harga);
        }
    }
}
