package com.example.shopproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Admin.DetailOrderActivity;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.R;

import java.util.List;

public class AdapterOrderMember extends RecyclerView.Adapter<AdapterOrderMember.HolderOrder> {
    List<PesanToAdmin> adminList;
    Context context;

    public AdapterOrderMember(List<PesanToAdmin> adminList, Context context) {
        this.adminList = adminList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterOrderMember.HolderOrder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order3, parent, false);
        return new HolderOrder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterOrderMember.HolderOrder holder, int position) {
        holder.nama.setText(adminList.get(position).getPnamaPemesan());
        holder.tanggal.setText(adminList.get(position).getTanggal());
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailOrderActivity.class);
                i.putExtra("key",adminList.get(position).getKey());
                i.putExtra("nama",adminList.get(position).getPnamaPemesan());
                i.putExtra("total",adminList.get(position).getTotal());
                i.putExtra("tgl", adminList.get(position).getTanggal());
                i.putExtra("role","member");
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return adminList.size();
    }

    public class HolderOrder extends RecyclerView.ViewHolder {
        TextView nama, tanggal,detail;
        public HolderOrder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama_order);
            tanggal = itemView.findViewById(R.id.tgl_order);
            detail = itemView.findViewById(R.id.tv_detail);
        }
    }
}
