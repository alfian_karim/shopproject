package com.example.shopproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Admin.DetailOrderActivity;
import com.example.shopproject.Model.PesanToAdmin;
import com.example.shopproject.PilihanLogin;
import com.example.shopproject.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.HolderOrder> {
    List<PesanToAdmin> adminList;
    Context context;
    boolean status = false;
    private DatabaseReference mDatabaseRef, mDatabaseRef2;

    public AdapterOrder(List<PesanToAdmin> adminList, Context context) {
        this.adminList = adminList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterOrder.HolderOrder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order2, parent, false);
        return new HolderOrder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterOrder.HolderOrder holder, int position) {
        if (adminList.get(position).getStatus().equals("ongoing")){
            holder.parent.setBackgroundResource(R.drawable.bg_order);
        }else {
            holder.parent.setBackgroundResource(R.color.biru);
            holder.selesai.setVisibility(View.GONE);
        }
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("PesananMember");
        mDatabaseRef2 = FirebaseDatabase.getInstance().getReference("SemuaPesananMember");

        PesanToAdmin pesan = new PesanToAdmin(adminList.get(position).getPesanan(), adminList.get(position).getTotal(),
                adminList.get(position).getPnamaPemesan(), adminList.get(position).getPpemail(), adminList.get(position).getTanggal(),
                "selesai");

        holder.nama.setText(adminList.get(position).getPnamaPemesan());
        holder.tanggal.setText(adminList.get(position).getTanggal());
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailOrderActivity.class);
                i.putExtra("key",adminList.get(position).getKey());
                i.putExtra("nama",adminList.get(position).getPnamaPemesan());
                i.putExtra("total",adminList.get(position).getTotal());
                i.putExtra("tgl", adminList.get(position).getTanggal());
                i.putExtra("role","admin");
                context.startActivity(i);

            }
        });
        holder.selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatabaseRef.child(adminList.get(position).getKey()).setValue(pesan);
                mDatabaseRef2.child(adminList.get(position).getKey()).setValue(pesan);

                Toast.makeText(context, "Berhasil merubah status pesanan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return adminList.size();
    }

    public class HolderOrder extends RecyclerView.ViewHolder {
        TextView nama, tanggal,detail;
        Button selesai;
        RelativeLayout parent;
        public HolderOrder(@NonNull View itemView) {
            super(itemView);

            parent= itemView.findViewById(R.id.parent);
            selesai = itemView.findViewById(R.id.btn_selesai);
            nama = itemView.findViewById(R.id.nama_order);
            tanggal = itemView.findViewById(R.id.tgl_order);
            detail = itemView.findViewById(R.id.tv_detail);
        }
    }
}
