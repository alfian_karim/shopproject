package com.example.shopproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Admin.UpdateBarang;
import com.example.shopproject.Member.KeranjangActivity;
import com.example.shopproject.Model.Barang;
import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.Model.Pesanan;
import com.example.shopproject.R;
import com.google.android.material.transition.Hold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterBarangBeli extends RecyclerView.Adapter<AdapterBarangBeli.HolderBarangBeli> {
    List<Barang> list ;
    Context context;
    int click = 0;
    HashMap<Integer,Integer> has_int = new HashMap<>();

    public AdapterBarangBeli(List<Barang> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderBarangBeli onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_barang, parent, false);
        return new HolderBarangBeli(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderBarangBeli holder, int position) {
        has_int.put(position,1);
        holder.tv_nama.setText(list.get(position).getNama());
        holder.tv_harga.setText("Rp."+list.get(position).getHarga());
        holder.ll_opsi.setVisibility(View.GONE);
        holder.tv_jml.setText("Jumlah Stok : "+list.get(position).getJumlah());
        holder.btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click = has_int.get(position);
                click++;
                has_int.put(position,click);
                Log.i("TAG", "onClick: "+click);
                holder.tv_jml_beli.setText(String.valueOf(has_int.get(position)));
            }
        });
        holder.btn_min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click = has_int.get(position);
                click--;
                has_int.put(position,click);
                if(has_int.get(position)<1){
                    has_int.put(position,1);
                    holder.tv_jml_beli.setText(String.valueOf(has_int.get(position)));
                }else{
                    holder.tv_jml_beli.setText(String.valueOf(has_int.get(position)));
                }
            }
        });
        holder.btn_pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pesanan pesanan = new Pesanan(list.get(position).getNama(), Integer.parseInt(holder.tv_jml_beli.getText().toString()), Integer.parseInt(list.get(position).getHarga()));
                KeranjangActivity.pesananList.add(pesanan);
                has_int.put(position,1);
                holder.tv_jml_beli.setText(String.valueOf(has_int.get(position)));
                Toast.makeText(context, "Barang Sudah di tambahkan di keranjang", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderBarangBeli extends RecyclerView.ViewHolder {
        TextView tv_nama,tv_harga,tv_jml;
        LinearLayout ll_opsi;
        ImageView btn_plus,btn_min;
        TextView tv_jml_beli;
        Button btn_pesan;
        View v;
        public HolderBarangBeli(@NonNull View itemView) {
            super(itemView);
            v = itemView;
            tv_nama = itemView.findViewById(R.id.nama_barang);
            tv_harga = itemView.findViewById(R.id.tv_harga);
            tv_jml = itemView.findViewById(R.id.tv_stok);
            ll_opsi = itemView.findViewById(R.id.ll_opsi);
            tv_jml_beli = itemView.findViewById(R.id.tv_jml);
            btn_min = itemView.findViewById(R.id.iv_min);
            btn_plus = itemView.findViewById(R.id.iv_plus);
            btn_pesan = itemView.findViewById(R.id.btn_pesan);

        }
    }
}
