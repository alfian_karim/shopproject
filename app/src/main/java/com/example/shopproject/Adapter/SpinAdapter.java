package com.example.shopproject.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.shopproject.R;

import java.util.List;

public class SpinAdapter extends ArrayAdapter {
    public SpinAdapter(@NonNull Context context, int resource ,@NonNull String[] objects) {
        super(context, resource, objects);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;
        if(position == 0){
            tv.setTextColor(getContext().getResources().getColor(R.color.abu));
        }
        return view;
    }
    @Override
    public boolean isEnabled(int position){
        if(position == 0){
            return  false;
        }else {
            return true;
        }
    }
}
