package com.example.shopproject.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopproject.Model.Pesanan;
import com.example.shopproject.R;

import java.util.List;

public class AdapterBarangSudahBeli extends RecyclerView.Adapter<AdapterBarangSudahBeli.HolderBarangs>{
    List<Pesanan> list;
    Context context;

    public AdapterBarangSudahBeli(List<Pesanan> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterBarangSudahBeli.HolderBarangs onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_detail_barang, parent, false);
        return new HolderBarangs(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBarangSudahBeli.HolderBarangs holder, int position) {
        holder.nama.setText(list.get(position).getNamaBarang());
        holder.jumlah.setText("x"+String.valueOf(list.get(position).getJumlah()));
        holder.harga.setText("Rp. "+list.get(position).getHarga());
        holder.total.setText("Rp. "+String.valueOf(list.get(position).getJumlah()*list.get(position).getHarga()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderBarangs extends RecyclerView.ViewHolder {
        TextView nama, jumlah, harga, total;
        public HolderBarangs(@NonNull View itemView) {
            super(itemView);

            total = itemView.findViewById(R.id.tv_total);
            nama = itemView.findViewById(R.id.tv_nama);
            jumlah = itemView.findViewById(R.id.tv_jumlah);
            harga = itemView.findViewById(R.id.tv_harga);
        }
    }
}
