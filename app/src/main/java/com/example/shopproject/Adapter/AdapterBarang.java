package com.example.shopproject.Adapter;

import android.app.AlertDialog;
import android.app.Presentation;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.shopproject.Admin.UpdateBarang;
import com.example.shopproject.Model.Barang;
import com.example.shopproject.Model.BarangRes;
import com.example.shopproject.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class AdapterBarang extends RecyclerView.Adapter<AdapterBarang.HolderBarang> {
    List<Barang> list = new ArrayList<>();
    Context context;
    private FirebaseStorage mStorageRef;
    private DatabaseReference mDatabaseRef;

    public AdapterBarang(List<Barang> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderBarang onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_barang2, parent, false);
        return new HolderBarang(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderBarang holder, int position) {
    holder.tv_nama.setText(list.get(position).getNama());
    holder.tv_harga.setText("Rp."+list.get(position).getHarga());
    Glide.with(context).load(list.get(position).getUrlImage()).into(holder.iv_barang);
    holder.ll_opsi.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mStorageRef = FirebaseStorage.getInstance();
            mDatabaseRef = FirebaseDatabase.getInstance().getReference(list.get(position).getJenis());
            final String selectedKey = list.get(position).getKey();

            PopupMenu popupMenuAlamat = new PopupMenu(context,holder.ll_opsi);
            popupMenuAlamat.getMenuInflater().inflate(R.menu.menu_barang, popupMenuAlamat.getMenu());
            popupMenuAlamat.show();
            popupMenuAlamat.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.edit:
                            Intent intent = new Intent(context, UpdateBarang.class);
                            intent.putExtra("nama", list.get(position).getNama());
                            intent.putExtra("harga", list.get(position).getHarga());
                            intent.putExtra("jenis", list.get(position).getJenis());
                            intent.putExtra("idSpinner", list.get(position).getIdSpinner());
                            intent.putExtra("jumlah", list.get(position).getJumlah());
                            intent.putExtra("image", list.get(position).getUrlImage());
                            intent.putExtra("key", list.get(position).getKey());
                            intent.putExtra("uri", list.get(position).getUriIamge());
                            context.startActivity(intent);
                            return true;
                        case R.id.hapus:
                            StorageReference imageRef = mStorageRef.getReferenceFromUrl(list.get(position).getUrlImage());
                            imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mDatabaseRef.child(selectedKey).removeValue();
                                    Toast.makeText(context, "Menghapus barang", Toast.LENGTH_SHORT).show();
                                    removeAt(position);
                                }
                            });
                            return true;
                        default:
                            return false;
                    }
                }
            });
        }
    });
    holder.tv_jml.setText("Jumlah Stok : "+list.get(position).getJumlah());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderBarang extends RecyclerView.ViewHolder {
        TextView tv_nama,tv_harga,tv_jml;
        ImageView iv_barang;
        LinearLayout ll_opsi;
        View v;
        public HolderBarang(@NonNull View itemView) {
            super(itemView);
            v = itemView;

            iv_barang = itemView.findViewById(R.id.iv_barang);
            tv_nama = itemView.findViewById(R.id.nama_barang);
            tv_harga = itemView.findViewById(R.id.tv_harga);
            tv_jml = itemView.findViewById(R.id.tv_stok);
            ll_opsi = itemView.findViewById(R.id.ll_opsi);

        }
    }

    public void removeAt(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }
}